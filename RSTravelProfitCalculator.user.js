// ==UserScript==
// @name         Reality's Travel Profit Calculator
// @namespace    profitPerMinuteCalc
// @version      1.02
// @description  Shows best place to travel at a given second
// @author       RealityShift[2001304]
// @match        https://www.torn.com/travelagency.php
// @require      https://gist.github.com/raw/2625891/waitForKeyElements.js
// @require      https://code.jquery.com/jquery-2.2.0.min.js
// @grant        GM_addStyle
// ==/UserScript==
// June 12th 2017 - Implemented numbers with commas for table.
// June 17th 2017 - Implemented color grid for table, color legend, and units stocked per country (data from DoctorN : https://chrome.google.com/webstore/detail/doctorn-for-torn/kfdghhdnlfeencnfpbpddbceglaamobk )
// June 18th 2017 - Implemented css to make it look like a proper torn table

(function() {
    'use strict';

    // Input your own API key
    let APIKEY = localStorage.rsApiKey || "";
    let LIVEDATA = "";
    // Array of JSON objects items to track for PPM
    let ITEMS = [{
            "name": "Dahlia",
            "item_id": "260",
            "location": "Mexico",
            "location2": "mexico",
            "buy_price": 300,
            "travel_time": 36,
						"low_supply": 100,
						"type": "Flower"
        },
        {
            "name": "Crocus",
            "item_id": "263",
            "location": "Canada",
            "location2": "canada",
            "buy_price": 600,
            "travel_time": 58,
						"low_supply": 100,
						"type": "Flower"
        },
        {
            "name": "Orchid",
            "item_id": "264",
            "location": "Hawaii",
            "location2": "hawaii",
            "buy_price": 700,
            "travel_time": 188,
						"low_supply": 300,
						"type": "Flower"
        },
        {
            "name": "Heather",
            "item_id": "267",
            "location": "United Kingdom",
            "location2": "uk",
            "buy_price": 5000,
            "travel_time": 222,
						"low_supply": 500,
						"type": "Flower"
        },
        {
            "name": "Ceibo Flower",
            "item_id": "271",
            "location": "Argentina",
            "location2": "argentina",
            "buy_price": 500,
            "travel_time": 234,
						"low_supply": 500,
						"type": "Flower"
        },
        {
            "name": "Edelweiss",
            "item_id": "272",
            "location": "Switzerland",
            "location2": "switzerland",
            "buy_price": 900,
            "travel_time": 246,
						"low_supply": 500,
						"type": "Flower"
        },
        {
            "name": "Peony",
            "item_id": "276",
            "location": "China",
            "location2": "china",
            "buy_price": 5000,
            "travel_time": 338,
						"low_supply": 500,
						"type": "Flower"
        },
        {
            "name": "Cherry Blossom",
            "item_id": "277",
            "location": "Japan",
            "location2": "japan",
            "buy_price": 500,
            "travel_time": 316,
						"low_supply": 500,
						"type": "Flower"
        },
        {
            "name": "African Violet",
            "item_id": "282",
            "location": "South Africa",
            "location2": "south-africa",
            "buy_price": 2000,
            "travel_time": 416,
						"low_supply": 600,
						"type": "Flower"
        },
        {
            "name": "Tribulus Omanense",
            "item_id": "385",
            "location": "Dubai",
            "location2": "uae",
            "buy_price": 6000,
            "travel_time": 380,
						"low_supply": 600,
						"type": "Flower"
        },
        {
            "name": "Banana Orchid",
            "item_id": "617",
            "location": "Cayman Islands",
            "location2": "cayman-islands",
            "buy_price": 4000,
            "travel_time": 50,
						"low_supply": 100,
						"type": "Flower"
        },
        {
            "name": "Sheep Plushie",
            "item_id": "186",
            "location": "City",
            "buy_price": 50,
            "travel_time": 0,
						"low_supply": 0,
						"type": "Plushie"
        },
        {
            "name": "Teddy Plushie",
            "item_id": "187",
            "location": "City",
            "buy_price": 50,
            "travel_time": 0,
						"low_supply": 0,
						"type": "Plushie"
        },
        {
            "name": "Kitten Plushie",
            "item_id": "215",
            "location": "City",
            "buy_price": 50,
            "travel_time": 0,
						"low_supply": 0,
						"type": "Plushie"
        },
        {
            "name": "Jaguar Plushie",
            "item_id": "258",
            "location": "Mexico",
            "location2": "mexico",
            "buy_price": 10000,
            "travel_time": 36,
						"low_supply": 100,
						"type": "Plushie"
        },
        {
            "name": "Wolverine Plushie",
            "item_id": "261",
            "location": "Canada",
            "location2": "canada",
            "buy_price": 30,
            "travel_time": 58,
						"low_supply": 100,
						"type": "Plushie"
        },
        {
            "name": "Nessie Plushie",
            "item_id": "266",
            "location": "United Kingdom",
            "location2": "uk",
            "buy_price": 200,
            "travel_time": 222,
						"low_supply": 500,
						"type": "Plushie"
        },
        {
            "name": "Red Fox Plushie",
            "item_id": "268",
            "location": "United Kingdom",
            "location2": "uk",
            "buy_price": 1000,
            "travel_time": 222,
						"low_supply": 500,
						"type": "Plushie"
        },
        {
            "name": "Monkey Plushie",
            "item_id": "269",
            "location": "Argentina",
            "location2": "argentina",
            "buy_price": 400,
            "travel_time": 234,
						"low_supply": 500,
						"type": "Plushie"
        },
        {
            "name": "Chamois Plushie",
            "item_id": "273",
            "location": "Switzerland",
            "location2": "switzerland",
            "buy_price": 400,
            "travel_time": 246,
						"low_supply": 500,
						"type": "Plushie"
        },
        {
            "name": "Panda Plushie",
            "item_id": "274",
            "location": "China",
            "location2": "china",
            "buy_price": 400,
            "travel_time": 338,
						"low_supply": 500,
						"type": "Plushie"
        },
        {
            "name": "Lion Plushie",
            "item_id": "281",
            "location": "South Africa",
            "location2": "south-africa",
            "buy_price": 400,
            "travel_time": 416,
						"low_supply": 600,
						"type": "Plushie"
        },
        {
            "name": "Camel Plushie",
            "item_id": "384",
            "location": "Dubai",
            "location2": "uae",
            "buy_price": 14000,
            "travel_time": 380,
						"low_supply": 600,
						"type": "Plushie"
        },
        {
            "name": "Stingray Plushie",
            "item_id": "618",
            "location": "Cayman Islands",
            "location2": "cayman-islands",
            "buy_price": 400,
            "travel_time": 50,
						"low_supply": 100,
						"type": "Plushie"
        },
        {
            "name": "Xanax",
            "item_id": "206",
            "location": "South Africa",
            "location2": "south-africa",
            "buy_price": 752508,
            "travel_time": 416,
						"low_supply": 600,
						"type": "Drug"
        }
    ];

    // Processes one item to find the cheapest bazaar price and store in the ITEMS array
    // INPUT: One itemID number
    // OUTPUT: One promise after outputting data to ITEMS array
    function processOneItem(item) {
        return new Promise((resolve, reject) => {
            $.getJSON('https://api.torn.com/market/' + ITEMS[item].item_id + '?selections=bazaar&key=' + APIKEY, function(json) {
                if (json && json.error) {
                    reject(json);
                } else {
                    let cheapest = 0;

                    // find cheapest bazaar price in json response
                    for (let key in json.bazaar) {
                        if (json.bazaar.hasOwnProperty(key)) {
                            if (cheapest === 0)
                                cheapest = json.bazaar[key].cost;
                            else
                                cheapest = (cheapest < json.bazaar[key].cost) ? cheapest : json.bazaar[key].cost;
                        }
                    }
                    ITEMS[item].cheapest_bazaar = cheapest;
                    resolve();
                }
            });
        });
    }

    // Sorts array's of JSON objects by a specified key
    function sorting(json_object, key_to_sort_by) {
        function sortByKey(a, b) {
            let x = a[key_to_sort_by];
            let y = b[key_to_sort_by];
            return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        }

        json_object.sort(sortByKey);
    }

    // Creates a button with onclick of ExecuteCalculations()
    function CreateButton(text, id, appendElement) {
        let btn = document.createElement("input");
        btn.setAttribute("type", "button");
        btn.setAttribute("value", text);
        btn.setAttribute("id", id);
        btn.setAttribute("class", "silver rs-button");
        if (appendElement)
            document.getElementById(appendElement).appendChild(btn);
        else
            return btn;
    }

    // Formats number with the style xxx,xxx,xxx
    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    // Creates color legend for the travel page
    function CreateColorLegend() {
        let legend = document.createElement("UL");
        legend.setAttribute("class", "rs-legend");
        legend.setAttribute("id", "rs-legend");

        let li1 = document.createElement("LI");
        li1.innerText = "Good Destination";
        let sp1 = document.createElement("SPAN");
        sp1.setAttribute("class","good");
        li1.appendChild(sp1);
        legend.appendChild(li1);

        let li2 = document.createElement("LI");
        li2.innerText = "Low Stock";
        let sp2 = document.createElement("SPAN");
        sp2.setAttribute("class","nogo");
        li2.appendChild(sp2);
        legend.appendChild(li2);

        let li3 = document.createElement("LI");
        li3.innerText = "Drug CD Will Expire";
        let sp3 = document.createElement("SPAN");
        sp3.setAttribute("class","drugcd");
        li3.appendChild(sp3);
        legend.appendChild(li3);

        document.getElementById("tab-menu4").appendChild(legend);

        let clear = document.createElement("DIV");
        clear.setAttribute("class","clear");
        document.getElementById("tab-menu4").appendChild(clear);
    }

    // Creates a table with all data in it
    function CreateTable() {
        const travelTableHeaderText = ["Item Type", "Item Name", "Item Bazaar Price", "Profit Per Minute", "Location", "Units Stocked"];
        const KEYS = ["type", "name", "cheapest_bazaar", "profit_per_minute", "location", "units_stocked"];

        CreateColorLegend();

        // Creates article
        let travelArticle = document.createElement("ARTICLE");
        travelArticle.setAttribute("class", "rs-widget travel-hub");
        travelArticle.setAttribute("id", "rs-travel-widget");

        // Creates header with info icon and text
        let travelHeader = document.createElement("HEADER");
        travelHeader.setAttribute("class", "rs-widget_header");
        let travelHeaderIcon = document.createElement("i");
        travelHeaderIcon.setAttribute("class", "info-icon");
        travelHeader.appendChild(travelHeaderIcon);
        travelHeader.append("  RealityShift's Travel Profits");
        travelArticle.appendChild(travelHeader);

        // Creates div for section and table
        let travelDiv = document.createElement("DIV");
        travelDiv.setAttribute("class", "rs-travel-hub-wrapper");
        travelArticle.appendChild(travelDiv);

        // Creates section
        let travelSection = document.createElement("SECTION");
        travelSection.setAttribute("class", "rs-travel-hub-content");
        travelDiv.appendChild(travelSection);

        // Creates the table
        let travelTable = document.createElement("TABLE");
        travelTable.setAttribute("id", "TravelData");
        travelSection.appendChild(travelTable);

        // Appends everything above to the page.
        document.getElementById("tab-menu4").appendChild(travelArticle);

        // Creates TR
        let travelTableTr = document.createElement("TR");
        travelTableTr.setAttribute("id", "TravelDataTR");
        travelTableTr.setAttribute("class", "nocolor");
        document.getElementById("TravelData").appendChild(travelTableTr);

        // Creates Table Header for each element in the array
        for (let count = 0; count < travelTableHeaderText.length; count++) {
            let travelTableHeader = document.createElement("TH");
            const travelTableTextNode = document.createTextNode(travelTableHeaderText[count]);
            travelTableHeader.appendChild(travelTableTextNode);
            document.getElementById("TravelDataTR").appendChild(travelTableHeader);
        }

        // Fills table with corresponding data
        // For each item in ITEMS
        for (let item in ITEMS) {
            if (item !== '0' && item !== '1' && item !== '2') {
                let trElement = document.createElement("TR");

                // For each key in KEYS
                for (let count = 0; count < KEYS.length; count++) {
                  let txtNode = 0;

                  // if looking at unit stocks
                  if (count === 5) {
                    const qty = GetSupplies(item);

                    txtNode = qty;

                    // Color the tr red if stock is low/none
                    if (qty<= ITEMS[item].low_supply )
                      trElement.setAttribute("class", "nogo");
                  }
                  else {
                    txtNode = ITEMS[item][KEYS[count]];
                  }

                  // Color the tr teal if you can't make a round trip before drug cd is ready
                  if (CheckDrugCooldown(item))
                    trElement.setAttribute("class", "drugcd");

                  let tdElement = document.createElement("TD");
                  // if key being used is cheapest_bazaar, format numbers with commas
                  if (count === 2 || count === 5)
                      txtNode = numberWithCommas(txtNode);

                  const txtElement = document.createTextNode(txtNode);
                  tdElement.appendChild(txtElement);
                  trElement.appendChild(tdElement);

                }

                document.getElementById("TravelData").appendChild(trElement);
            }
        }
    }

    // Check if drug cd exists and compare to flight time
    function CheckDrugCooldown(item) {
      const drugIcons = ["icon49", "icon50", "icon51", "icon52", "icon53"];
      let drugData;

      drugIcons.forEach(d => {
        if (document.getElementById(d))
          drugData = document.getElementById(d);
      });

      if (drugData !== "" && drugData !== null && drugData !== undefined) {
        const timer = drugData.title.split('>')[5].split('<')[0].split(':');
        const drugCd = parseInt(timer[0]) * 60 + parseInt(timer[1]);
        const flightTime = ITEMS[item].travel_time;

        if (drugCd < flightTime) {
          return true;
        }
      }

      return false;
    }

    // Pulls supplies from mrkishi's live hub data
    function GetSupplies(item) {
      const location = ITEMS[item].location2;
      const allSupplies = LIVEDATA[location].supplies;
      const curItemId = parseInt(ITEMS[item].item_id);
      const lowQty = ITEMS[item].low_supply;

      const qty = allSupplies.find(s => s.item_id === curItemId).units;

      return qty;
    }

    // Creates API form if API key is missing
    function CreateApiForm() {
        let travelCalcApiForm = document.createElement("form");
        travelCalcApiForm.setAttribute("action", "javascript:ValidateApiKey()");
        travelCalcApiForm.setAttribute("id", "TravelCalcApiForm");

        let labelApi = document.createElement("label");
        labelApi.setAttribute("for", "txtApiKey");
        labelApi.innerText = "API Key";
        labelApi.setAttribute("style", "font-weight: bold;");

        let txtApiKey = document.createElement("input");
        txtApiKey.setAttribute("type", "text");
        txtApiKey.setAttribute("id", "txtApiKey");
        txtApiKey.setAttribute("style", "padding: 5px 1px 5px 9px; line-height: 14px; width: 150apx; vertical-align: middle; font-weight: 400;");

        let submitApiKey = document.createElement("input");
        submitApiKey.setAttribute("type", "button");
        submitApiKey.setAttribute("id", "submitapikey");
        submitApiKey.setAttribute("value", "Submit");
        submitApiKey.setAttribute("class", "btn-wrap silver rs-button");
        submitApiKey.addEventListener("click", ValidateApiKey, false);

        travelCalcApiForm.appendChild(labelApi);
        travelCalcApiForm.appendChild(txtApiKey);
        travelCalcApiForm.appendChild(submitApiKey);

        $('#tab-menu4').after(travelCalcApiForm);
    }

    // Creates API form if API key is missing or creates calculate button if not
    function CheckApiKey() {
        if (APIKEY === "") {
            CreateApiForm();
        } else {
            CreateButton('Calculate Profit Per Minute', 'TravelCalcButton', 'tab-menu4');
            $('#TravelCalcButton').bind("click", ExecuteCalculations);
        }
    }

    // Executes a profile query to test API key for validity
    function ValidateApiKey() {
        const tmpApi = document.getElementById('txtApiKey').value;
        $('#TravelCalcApiForm').remove();
        $.getJSON('https://api.torn.com/user/?selections=basic&key=' + tmpApi, function(json) {
            if (json && json.error) {
                ApiError(json.error.code);
                if (json.error.code === 5) {
                    CreateApiForm();
                    $('#TravelCalcApiForm').append('<p id="rs-api-error">You IP is temporarily banned due to abuse</p>');
                }
            } else {
                console.log('saving apikey');
                APIKEY = tmpApi;
                localStorage.rsApiKey = APIKEY;
                CheckApiKey();
            }
        });
        return false;
    }

    // Sets error message depending on the error code received from API query
    function ApiError(code) {
        let message = "Unknown error.";
        switch (code) {
            case 1:
                message = "Key is empty";
                break;
            case 2:
                APIKEY = "";
                localStorage.rsApiKey = APIKEY;
                $("#TravelCalcButton").remove();
                CheckApiKey();
                message = "Incorrect Key: Please enter a valid API key.";
                $('#TravelCalcApiForm').append('<p id="rs-api-error">' + message + '</p>');
                break;
            case 3:
                message = "Wrong type";
                break;
            case 4:
                message = "Wrong fields";
                break;
            case 5:
                message = "Too many requests: Please wait 60 seconds before attempting a refresh.";
                $("#TravelCalcButton").prop("disabled", true);
                setTimeout(function() {
                    $("#TravelCalcButton").prop("disabled", false);
                }, 60000);
                break;
            case 6:
                message = "Incorrect ID";
                break;
            case 7:
                message = "Incorrect ID-entity relation";
                break;
            case 8:
                message = "IP block";
                break;
            case 9:
                message = "API Disabled";
                break;
            case 10:
                message = "Key owner is in federal jail";
                break;
            default:
                message = "Uknown error";
                CreateApiForm();
                break;
        }
        if (code !== 2)
            $('#tab-menu4').append('<p id="rs-api-error">' + message + '</p>');
    }

    function ChangeCalcButton(text, disabledState) {
        $("#TravelCalcButton").prop('value', text);
        $("#TravelCalcButton").prop("disabled", disabledState);
    }

    // Main function
    function ExecuteCalculations() {
        // Removes previous table
        if (document.getElementById('TravelData')) {
          $('#rs-travel-widget').remove();
          $('#rs-legend').remove();
        }

        ChangeCalcButton('Calculating...', true);

        // Get an array of promises
        const promises = Object.keys(ITEMS).map(item => processOneItem(item));

        Promise.all(promises).then(results => {
            // calculates the profit per minute for each item in the ITEMS array
            for (let item in ITEMS) {
                ITEMS[item].profit_per_minute = Math.floor((ITEMS[item].cheapest_bazaar - ITEMS[item].buy_price) / ITEMS[item].travel_time);
            }

            sorting(ITEMS, 'profit_per_minute');

            CreateTable();
            ChangeCalcButton('Refresh Data', false);
        }).catch(err => {
            ChangeCalcButton('value', 'Calculate Profit Per Minute', true);

            console.log('error: ', err);
            ApiError(err.error.code);
        });
    }

    // css style for page
    GM_addStyle(
				'.rs-widget { box-shadow: rgba(0, 0, 0, 0.25) 0px 1px 3px; margin: 10px 0px; animation: a 0.5s ease-in-out both; overflow: hidden; border-radius: 5px; }' +
				'.rs-widget.travel-hub header { background-color: #6075ff; }' +
				'.rs-widget_header { color: rgb(255, 255, 255); font-size: 13px; line-height: 18px; letter-spacing: 1px; vertical-align: bottom; text-shadow: rgba(0, 0, 0, 0.65) 1px 1px 2px; background-color: rgb(45, 45, 45); background-image: linear-gradient(90deg, transparent 50%, rgba(0, 0, 0, 0.07) 0px); background-size: 4px; padding: 6px 10px; }' +
				'.rs-travel-hub-wrapper { background-color: #f2f2f2; display: flex; align-items: stretch; flex-grow: 1; max-height: 80vh; margin: 0px; }' +
				'.rs-travel-hub-content { flex-grow: 1; overflow-y: auto; margin: 0px; }' +
				'#TravelData { font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; border-collapse: collapse; width: 100%; }' +
				'#TravelData td, #TravelData th { border: 1px solid #ddd; padding: 8px; }' +
				'#TravelData tr { background-color: #a1f8a1; }' +
				'#TravelData tr:hover { background-color: #84cc84; }' +
				'#TravelData tr.nocolor { background-color: #ffffff; }' +
				'#TravelData tr.nogo {background-color: #f78f8f; }' +
				'#TravelData tr.nogo:hover {background-color: #cc7474;}' +
				'#TravelData tr.drugcd { background-color: #adcdce; }' +
				'#TravelData tr.drugcd:hover {background-color: #8faaaa;}' +
				'.rs-button { cursor: pointer; padding: 0px 10px; font-weight: bold; height: 24px; margin-top: 10px; line-height: 24px; text-align: center; background: url(/images/v2/main/buttons/buttons_desktop.png?v=1496754769965) left top no-repeat; }' +
				'.rs-button div.btn { padding: 0 10px 0 7px !important }' +
        '.rs-legend { list-style: none; margin-top: 10px; }' +
        '.rs-legend li { float: left; margin-right: 10px; }' +
        '.rs-legend span { border: 1px solid #ccc; float: left; width: 12px; height: 12px; margin-right: 2px; }' +
        '.rs-legend .good { background-color: #a1f8a1; }' +
        '.rs-legend .nogo { background-color: #f78f8f; }' +
        '.rs-legend .drugcd { background-color: #6895d6; }' +
        ''
    );

    // EventSource data used for supplies count in each country
    window.sse = new EventSource('https://doctorn-mrkishi.rhcloud.com/travel-hub/live');
    window.sse.addEventListener('load', (l) => { const r = JSON.parse(l.data); LIVEDATA = r; });

    CheckApiKey();
})();
