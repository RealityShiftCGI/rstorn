// ==UserScript==
// @name         Bookie Withdraw
// @namespace    OttBot-Bookie-Bank
// @version      4.55
// @description  Allows for rapid withdrawal/deposit of funds from the bookie
// @author       0tt3r / sullengenie / RealityShift
// @include      *.torn.com/*
// @require      https://code.jquery.com/jquery-2.2.0.min.js
// @require      https://gist.github.com/raw/2625891/waitForKeyElements.js
// @grant        GM_xmlhttpRequest
// @grant        GM_addStyle
// @grant        GM_setValue
// @grant        GM_getValue
// @updateURL	 http://torn.teccatechsoftware.com/bookiewithdraw.user.js
// @downloadURL	 http://torn.teccatechsoftware.com/bookiewithdraw.user.js
// ==/UserScript==
// March 15 2017 - removed auto clicks due to rule changes in Torn.
// March 15 2017 - added bookie bank capabilities.
// March 16 2017 - Fixed a bug with DepositAll button not re-evaluating money received after page load.
// March 17 2017 - Fixed a bug where new money acquired wasn't taken into account for the bet causing withdraws of large cash.
// March 20 2017 - Wrapped code in strict mode and added bookie timer alarm feature. Will finish migrating vars to let/const when time allows.
// May 11 2017 - Implemented multi bookie support. Added settings page to preferences.php. Cleaned up some bugs and code. This is a beta push.
// June 13 2017 - Fixed bug to not lose old bookie when upgrading script. Deploying new update.
// June 23 2017 - Added title change for bookie pages so you can tell which bookie is on which tab.
// June 23 2017 - Made bookie bank show which bookie bank number it was in dropdown selector if it's a bookie bank.
// October 20 2017 - Implemented AJAX responses and lowest multi selector.
// June 28 2018 - Fixed page loads on events page

(function() {
    'use strict';

    let moneyInBet = 0;
    let moneyOnHand = 0;
    let newBet = 0;

    // ---------------------------------------- GUI ----------------------------------------

    function AddGmStyleToPage() {
        GM_addStyle(
            '#OttBot-panel { line-height: 2em }' +
            '#OttBot-panel label { background-color: rgba(200, 195, 195, 1); padding: 2px; border: 1px solid #fff; border-radius: 5px }' +
            '#OttBot-panel input { margin-right: 5px; vertical-align: text-bottom }' +
            '#OttBot-panel input[type="number"] { vertical-align: baseline; line-height: 1.3em }' +
            '#OttBot-panel { padding: 4px; }' +
            '.otter-button { cursor: pointer }' +
            '.otter-button { font-weight: bold }' +
            '.otter-button { padding: 0px 10px }' +
            '.otter-button div.btn { padding: 0 10px 0 7px !important }');
    }

    // ----------------------------------- GUI FUNCTIONS -----------------------------------

    // Creates bookie notice
    function bookieInfoNotice(msg) {
        (createInfoNotice(msg, 'bookieFrame', 'green')).insertBefore('.bet-wrap.m-top10');

        timeoutIntoNotice('bookieFrame');
    }

    // Creates a nice looking botton from tornstats
    function createButton(text, special) {
        if (special == "bank") {
            return $('<div>', {
                'class': 'btn-wrap silver otter-button',
                'style': 'float: right'
            }).append($('<div>', {
                'type': 'button',
                text: text
            }));
        } else {
            return $('<div>', {
                'class': 'btn-wrap silver otter-button'
            }).append($('<div>', {
                'type': 'button',
                text: text
            }));
        }
    }

    // Creates a link on the left panel to the bookie bank
    function createBookieBankLi() {
        for (let i = bookieBankQty; i > 0; i--) {
            //console.log("i: " + i);
            var address = GM_getValue("bookie-bank-address-" + i, "-1");
            //console.log('address: ' + address);
            if (i === 1 && address === "-1") {
                address = "https://www.torn.com/bookie.php";
            }

            // Loops to add all bookie links on left panel
            if (address !== null && address !== undefined && address !== "-1") {
                const text = "Bookie Bank " + i;
                let bbLink = document.createElement('a');
                let bbLI = document.createElement('li');
                let bbDiv = document.createElement('div');
                let sideBarCasino = document.getElementById('nav-casino');

                // Checks for casino in the side bar and adds the link
                if (sideBarCasino) {
                    bbLink.setAttribute('href', address);

                    bbLink.innerHTML = '<i class="left"></i><span class="border-l"></span><span class="border-r"></span><span class="list-link-name">' + '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &#8226; ' + text + '</span>';

                    bbDiv.setAttribute('class', 'list-link');
                    bbDiv.appendChild(bbLink);
                    bbLI.appendChild(bbDiv);

                    sideBarCasino.parentNode.insertBefore(bbLI, sideBarCasino.nextSibling);
                }
            }
        }
    }

    // Edits the info panel
    function infoPanel(action, changeAmount) {
        const msg = ("Successful " + action + " for " + numberFormat(changeAmount, "###,###,###,###"));
        console.log("Change amount: ", changeAmount);

        if (document.getElementById("bookieWarning")) {
            document.getElementById('bookieWarning').innerText = msg;
        } else {
            bookieInfoNotice(msg);
        }
    }

    // Creats an info notice
    function createInfoNotice(msg, id, color) {
        return ($('<div class="info-msg-cont border-round m-top10 ' + color + '" id="' + id + '"><div class="info-msg border-round">' +
            '<i class="info-icon"></i><div class="delimiter"><div id="bookieWarning" class="msg right-round">' + msg +
            '</div></div></div></div>'));
    }

    // Timeout to remove message
    function timeoutIntoNotice(id) {
        setTimeout(function() {
            $('#' + id).remove();
        }, 10000);
    }

    // --------------------------------- FUNCTION HELPERS ----------------------------------

    // Checks the timer on the bookie bank and creates a link if within 24 hours of closing
    function checkBookieTimers() {
        const curTime = new Date();

        // Loops each bookie bank to check timer
        for (let i = 1; i < bookieBankQty + 1; i++) {
            const bookieEndTime = GM_getValue("bookie-bank-" + i + "-timeout", "-1");

            // if timer is exists
            if (bookieEndTime !== "-1") {
                // if timer is over due, set to non-existant
                if (curTime.getTime() > bookieEndTime) {
                    GM_setValue("bookie-bank-" + i + "-timeout", "-1");
                }

                // if date is within 30 hours
                if (curTime.getTime() > (bookieEndTime - 86400000)) {
                    console.log(GM_setValue("bookie-bank-address-" + i, location.href));
                    (createInfoNotice('Your bookie bank ' + i + ' is expiring within 24 hours! Please move your money ASAP to avoid accidentally losing it.', 'bookieEndingNotice', 'red')).insertAfter('.content-title');
                }
            }
        }
    }

    // changes page title to bookie bank number
    function setTitleIfBookieBank(curUrl){
      for (let i = bookieBankQty; i > 0; i--) {
        const address = GM_getValue("bookie-bank-address-" + i);

        if (curUrl === address){
          document.title = "Bookie Bank #" + i;
          var multi = document.getElementById('bookie-bank-set-multi');
          multi.selectedIndex = (i-1);
        }
      }
    }

    function customWithdraw() {
        withdraw(parseInt($('input.deposit').val().replace('$', '').replace(/[^\d\.\-\ ]/g, '')));
    }

    function determineLowestMultiBet() {
        //$('div.bookie-main-wrap.m-top10 > div.bet-wrap.m-top10 > ul > li:nth-child(1) > ul > li.bet > div.input-wrap > div.input-money-group.success > input').val(newBet);
        const numberOfChildren = $('div.bookie-main-wrap.m-top10 > div.bet-wrap.m-top10 > ul').children().length;
        var teamWithLowestMulti = 999;
        var lowestMulti = 999;

        for (let i = 1; i < numberOfChildren; i++) {
            var tmp = $('div.bookie-main-wrap.m-top10 > div.bet-wrap.m-top10 > ul > li:nth-child(' + i + ') > ul > li.multiplier').text();
            tmp = tmp.split(":")[1];
            var tempMulti = tmp.trim();
            if (parseFloat(tempMulti) < lowestMulti) {
                teamWithLowestMulti = i;
                lowestMulti = parseFloat(tempMulti);
            }
        }

        return teamWithLowestMulti;
    }

    // Deposit money into bookie
    function deposit(amount) {
        const teamNumber = determineLowestMultiBet();
        let moneyOnHand = parseInt($("#FtcMA > span.m-hide").text().replace('$', '').replace(/[^\d\.\-\ ]/g, '').trim());
        if (isNaN(amount))
            amount = 0;
        if (amount > moneyOnHand)
            amount = moneyOnHand;
        newBet = moneyInBet + amount;
        if (newBet > 1000000000) {
            newBet = 1000000000;
            amount = newBet - moneyInBet;
        }

        //$('div.bookie-main-wrap.m-top10 > div.bet-wrap.m-top10 > ul > li:nth-child(1) > ul > li.bet > div.input-wrap > div.input-money-group.success > input').val(newBet);

        const cookieId = getCookie('rfc_v');
        const bookieId = parseInt(location.href.match(/\d+$/)[0], 10).toString();

        // POST data to AJAX to deposit money in bookie
        const formData = {
            "ID": bookieId,
            "ajax_action": "true",
            "amount": newBet.toString(),
            "confirmed": "1",
            "step": "addbet",
            "team": teamNumber.toString()
        };
        $.post('https://www.torn.com/bookie.php?rfcv=' + cookieId, formData)

        // Update visuals
        $("#FtcMA > span.m-hide").text("$" + numberWithCommas(moneyOnHand - amount));
        $('div.bookie-main-wrap.m-top10 > div.bet-wrap.m-top10 > ul > li:nth-child(' + teamNumber + ') > ul > li.bet > div.success-wrap > span.amount-wrap').text(numberWithCommas(newBet));

        var element = $('div.bookie-main-wrap.m-top10 > div.bet-wrap.m-top10 > ul > li:nth-child(' + teamNumber + ')')[0];
        element.setAttribute("class", "bg-green");
        element = $('div.bookie-main-wrap.m-top10 > div.bet-wrap.m-top10 > ul > li:nth-child(' + teamNumber + ') > ul > li.bet > div.input-wrap')[0];
        element.setAttribute("style", "display: none");
        element = $('div.bookie-main-wrap.m-top10 > div.bet-wrap.m-top10 > ul > li:nth-child(' + teamNumber + ') > ul > li.bet > div.success-wrap')[0];
        element.setAttribute("style", "display: block");
        element = $('div.bookie-main-wrap.m-top10 > div.bet-wrap.m-top10 > ul > li:nth-child(' + teamNumber + ') > ul > li.confirm-bet')[0];
        element.setAttribute("style", "display: none");

        moneyOnHand += moneyInBet - newBet;
        moneyInBet += amount;
        console.log('After trans');
        console.log('Money in bet:', moneyInBet);
        console.log('Money on hand:', moneyOnHand);

        infoPanel("deposit", "$" + amount + ".");
    }

    // Formats numbers with commas. Not sure if needed since javascript has built in formatter
    function numberWithCommas(x) {
        let parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    function customDeposit() {
        deposit(parseInt($('input.deposit').val().replace('$', '').replace(/[^\d\.\-\ ]/g, '')));
    }

    function depositAll() {
        moneyOnHand = parseInt($("#FtcMA > span.m-hide").text().replace('$', '').replace(/[^\d\.\-\ ]/g, '').trim());
        deposit(moneyOnHand);
    }

    function deposit500k() {
        deposit(500000);
    }

    function deposit1m() {
        deposit(1000000);
    }

    function deposit5m() {
        deposit(5000000);
    }

    function deposit10m() {
        deposit(10000000);
    }

    function deposit25m() {
        deposit(25000000);
    }

    function deposit50m() {
        deposit(50000000);
    }

    function deposit100m() {
        deposit(100000000);
    }

    // Sets current bookie as the bookie bank which creates a panel on the left nav menu
    function setBookieBank() {
        const multi = document.getElementById('bookie-bank-set-multi');
        const qty = multi.options[multi.selectedIndex].text;
        console.log("bookie-bank-address-" + qty + " || " + location.href);
        GM_setValue("bookie-bank-address-" + qty, location.href);

        let bookieTime = new Date();
        const timeToAdd = parseInt(($('#mainContainer > div.content-wrapper.m-left20.left > div.bookie-main-wrap.m-top10 > div.info-msg-cont.m-top10 > div.info-msg > div.delimiter > div.msg > span.time')).attr("timer"));
        bookieTime.setTime(bookieTime.getTime() + timeToAdd * 1000);
        GM_setValue("bookie-bank-" + qty + "-timeout", bookieTime.getTime());

        bookieInfoNotice("This bookie has been saved as your bookie bank " + qty + " ! :D");
    }

    function withdraw(amount) {
        const teamNumber = determineLowestMultiBet();
        let moneyOnHand = parseInt($("#FtcMA > span.m-hide").text().replace('$', '').replace(/[^\d\.\-\ ]/g, '').trim());
        if (isNaN(amount))
            amount = 0;
        newBet = moneyInBet - amount;
        if (newBet < 0) {
            newBet = 0;
            amount = moneyInBet;
        }

        const cookieId = getCookie('rfc_v');
        const bookieId = parseInt(location.href.match(/\d+$/)[0], 10).toString();

        // Posting AJAX response to withdraw money from bookie
        const formData = {
            "ID": bookieId,
            "ajax_action": "true",
            "amount": newBet.toString(),
            "confirmed": "1",
            "step": "addbet",
            "team": teamNumber.toString()
        };
        $.post('https://www.torn.com/bookie.php?rfcv=' + cookieId, formData)

        // Update visuals
        $("#FtcMA > span.m-hide").text("$" + numberWithCommas(moneyOnHand + amount));
        $('div.bookie-main-wrap.m-top10 > div.bet-wrap.m-top10 > ul > li:nth-child(' + teamNumber + ') > ul > li.bet > div.success-wrap > span.amount-wrap').text(numberWithCommas(newBet));

        moneyInBet -= amount;
        moneyOnHand += amount;
        console.log('After trans');
        console.log('Money in bet:', moneyInBet);
        console.log('Money on hand:', moneyOnHand);

        infoPanel("withdraw", "$" + amount + ".");
    }

    function withdrawAll() {
        withdraw(moneyInBet);
    }

    function withdraw500k() {
        withdraw(500000);
    }

    function withdraw1m() {
        withdraw(1000000);
    }

    function withdraw5m() {
        withdraw(5000000);
    }

    function withdraw10m() {
        withdraw(10000000);
    }

    function withdraw25m() {
        withdraw(25000000);
    }

    function withdraw50m() {
        withdraw(50000000);
    }

    function withdraw100m() {
        withdraw(100000000);
    }

    // --------------------------------------- END -----------------------------------------



    //Create a control panel for OttBot-PersonalPrice
    function LoadBookieBank() {
        AddGmStyleToPage();
        console.log("Entered init function");
        let $OttBotPanel = $('#OttBot-panel');

        if ($OttBotPanel.length !== 0) {
            $OttBotPanel.empty();
        } else {
            $OttBotPanel = $('<div>', {
                id: 'OttBot-panel'
            });
            $('div.bet-wrap.m-top10').before($OttBotPanel);
        }

        let $title = $('<div>', {
            'class': 'title-black m-top10 title-toggle tablet active top-round',
            text: 'Only uses top line! Do not use for actual betting!'
        });
        $('#div.bet-wrap.m-top10').before($title);
        const $infoBar = $('<div>', {
            'class': 'title-white m-top10 title-toggle tablet active top-round',
            'id': 'bookieWarning',
            text: 'Please click a button to start.'
        });
        $('#div.bet-wrap.m-top10');

        let $panel = $('<div>', {
            'class': 'cont-gray10 bottom-round cont-toggle'
        });
        $('div.bet-wrap.m-top10').before($panel);

        $OttBotPanel.append($title).append($panel);
        $panel.append('<input class="deposit" type="text" style="width:150px;"></input>');
        const button = createButton('Deposit').click(customDeposit);
        const button2 = createButton('Withdraw').click(customWithdraw);
        const button4 = createButton('Deposit All').click(depositAll);
        const buttonAll = createButton('Withdraw all').click(withdrawAll);
        const button3 = createButton('Set As Bookie Bank', "bank").click(setBookieBank);
        $panel.append('<select id=\"bookie-bank-set-multi\" name=\"bookie-bank-set-multi\" style=\"float:right\"></select>');
        for (let i = 1; i < bookieBankQty + 1; i++) {
            console.log("setting options: " + i);
            let option = document.createElement("option");
            option.text = i;
            option.value = i;
            let select = document.getElementById("bookie-bank-set-multi");
            select.appendChild(option);
        }

        $panel.append(button3);
        $panel.append('<div></div>');
        $panel.append(button);
        $panel.append(button2);
        $panel.append(button4);
        $panel.append(buttonAll);
        $panel.append('<br>');

        const withdrawNames = [
          {
            "name": "Withdraw 500k",
            "functionName": withdraw500k
          },
          {
            "name": "Withdraw 1m",
            "functionName": withdraw1m
          },
          {
            "name": "Withdraw 5m",
            "functionName": withdraw5m
          },
          {
            "name": "Withdraw 10m",
            "functionName": withdraw10m
          },
          {
            "name": "Withdraw 25m",
            "functionName": withdraw25m
          },
          {
            "name": "Withdraw 50m",
            "functionName": withdraw50m
          },
          {
            "name": "Withdraw 100m",
            "functionName": withdraw100m
          }
        ];

        withdrawNames.forEach(item => {
          const button = createButton(item.name).click(item.functionName);
          $panel.append(button);
        });

        const depositNames = [
          {
            "name": "Deposit 500k",
            "functionName": deposit(500000)
          },
          {
            "name": "Deposit 1m",
            "functionName": deposit1m
          },
          {
            "name": "Deposit 5m",
            "functionName": deposit5m
          },
          {
            "name": "Deposit 10m",
            "functionName": deposit10m
          },
          {
            "name": "Deposit 25m",
            "functionName": deposit25m
          },
          {
            "name": "Deposit 50m",
            "functionName": deposit50m
          },
          {
            "name": "Deposit 100m",
            "functionName": deposit100m
          }
        ];

        depositNames.forEach(item => {
          const button = createButton(item.name).click(item.functionName);
          $panel.append(button);
        });
        $panel.append('<br>');

        $panel.append($infoBar);

        moneyInBet = parseInt($('#mainContainer > div.content-wrapper.m-left20.left > div.bookie-main-wrap.m-top10 > div.bet-wrap.m-top10 > ul > li.row-wrap.bg-green > ul > li.bet > div.success-wrap > span.amount-wrap').text().replace(/[^\d\.\-\ ]/g, '').trim());
        moneyOnHand = parseInt($("#FtcMA > span.m-hide").text().replace('$', '').replace(/[^\d\.\-\ ]/g, '').trim());

        if (isNaN(moneyInBet)) moneyInBet = 0;
    }

    function createSettingsPage() {
        console.log("Entered create settings page function");
        $(".headers").children("div.clear").before("<li class=\"delimiter\"></li>\n" +
            "<li class=\"c-pointer left-bottom-round\" data-title-name=\"Bookie Bank Settings\" " +
            "id=\"bookie-bank\">\n" +
            "<a class=\"t-gray-6 bold h bookie-bank\">Bookie Bank Settings</a>\n" +
            "</li>");

        // Create settings page/pane for bookie bank
        let page = "</div><div id=\"bookie-bank-page\" class=\"prefs-cont left ui-tabs-panel ";
        page += "ui-widget-content ui-corner-bottom\" aria-labelledby=\"ui-id-33\" ";
        page += "role=\"tabpanel\" aria-expanded=\"true\" aria-hidden=\"true\" style=\"display: none;\">";
        page += "\t<div class=\"inner-block\">";
        page += "\t\t\t<div>";
        page += "\t\t\t\t<br><input type=\"checkbox\" id=\"bookie-enabled\" name=\"bookie-enabled\" value=\"bookie-enabled\" " + selected + "> Check this if you want the bookie bank feature enabled.";
        page += "\t\t\t</div>";
        page += "\t\t<div class=\"m-top3\" id=\"bookie-bank-selector\">";
        page += "\t\t<form class=\"m-top10\" action=\"\" id=\"bookie-bank-form\">";
        page += "\t\tSelect how many bookie banks you would like to have: ";
        page += "\t\t\t<select id=\"bookie-bank-multi\" name=\"bookie-bank-multi\">";
        page += "\t\t\t\t<option value=\"val1\">1</option>";
        page += "\t\t\t\t<option value=\"val2\">2</option>";
        page += "\t\t\t\t<option value=\"val3\">3</option>";
        page += "\t\t\t\t<option value=\"val4\">4</option>";
        page += "\t\t\t\t<option value=\"val5\">5</option>";
        page += "\t\t\t\t<option value=\"val6\">6</option>";
        page += "\t\t\t\t<option value=\"val7\">7</option>";
        page += "\t\t\t\t<option value=\"val8\">8</option>";
        page += "\t\t\t\t<option value=\"val9\">9</option>";
        page += "\t\t\t\t<option value=\"val10\">10</option>";
        page += "\t\t\t</select>";
        page += "\t\t</div>";
        page += "\t\t\t<div style=\"height:5px;\"></div>";
        page += "\t\t\t<div class=\"btn-wrap silver change\" id=\"bookie-button-wrap\">";
        page += "\t\t\t\t<div class=\"btn\" id=\"bookie-bank-submit\">";
        page += 'SAVE';
        page += "\t\t\t\t</div>";
        page += "\t\t\t</div>";
        page += "\t\t</form>";
        page += "\t</div>";
        page += "</div>";

        // Put 'page' somewhere between similar panes
        $("#management").after(page);

        // Hides/shows panel depending on if button is pushed or not. Also changes button to enabled
        $("input:checkbox[name='bookie-enabled']").change(function() {
            // New state
            console.log("Checkbox changed");
            $("div#bookie-button-wrap").removeClass("disable");
            if (this.checked) {
                selected = "checked";
                (document.getElementById('bookie-bank-selector')).style.display = "block";
            } else {
                selected = "";
                (document.getElementById('bookie-bank-selector')).style.display = "none";
            }
        });

        // Renables button if selection is changed
        $("select#bookie-bank-multi").change(function() {
            $("div#bookie-button-wrap").removeClass("disable");
        });

        // Add page to settingspage
        $("li#bookie-bank").click(function() {
            // Just the fancy click on the left-side menu
            $("li.ui-tabs-active").removeClass("ui-state-active").removeClass("ui-tabs-active");
            $("li#bookie-bank").addClass("ui-tabs-active").addClass("ui-state-active");
            // Remove current pane and show Bookie Bank page
            $("div.prefs-cont[aria-hidden='false']").css("display", "none").attr("aria-expanded", "false").attr("aria-hidden", "true");
            $("#bookie-bank-page").css("display", "table-cell").attr("aria-expanded", "true").attr("aria-hidden", "false");
            // Bind an eventhandler to the new submit button
            $("div#bookie-bank-submit").on("click", clickSaveButton);
            // If another link is clicked remove the pane (revert to it's original state)
            $("li.c-pointer").children("a").click(function() {
                $("li#bookie-bank").removeClass("ui-state-active").removeClass("ui-tabs-active");
                $("#bookie-bank-page").css("display", "none").attr("aria-expanded", "false").attr("aria-hidden", "true");
            });
        });

        // Saves settings to GM and disables button
        function clickSaveButton() {
            console.log('button clicked');
            $("div#bookie-button-wrap").addClass("disable");
            if (selected) {
                GM_setValue("bookie-bank-enabled", "checked");

                const multi = document.getElementById('bookie-bank-multi');
                const qty = multi.options[multi.selectedIndex].text;
                GM_setValue("bookie-bank-qty", qty);

                removeBookieBanks(qty);
            } else {
                GM_setValue("bookie-bank-enabled", "");
                removeBookieBanks(0);
            }
        }

        // Sets GM values of bookie to -1
        function removeBookieBanks(qty) {
            for (let i = 1; i <= parseInt(qty); i++) {
                console.log("setting bookie: " + i);
                GM_setValue("bookie-bank-address-" + i, "-1");
                GM_setValue("bookie-bank-" + i + "-timeout", "-1");
            }
        }

        if (selected === "") {
            (document.getElementById("bookie-enabled")).checked = true;
        }
        let tmp = document.getElementById('bookie-bank-multi');
        tmp.selectedIndex = bookieBankQty - 1;
    }

    // ############################## MAIN PROGRAM ##############################

    let selected = GM_getValue("bookie-bank-enabled", "checked");
    const bookieBankQty = parseInt(GM_getValue("bookie-bank-qty", "1"));

    // If bookie bank is enabled, run script
    // Always load settings page if preferences.php
    if (selected === "checked") {
        createBookieBankLi();

        if (document.location.href.match(/\/bookie.php*/)) {
            waitForKeyElements("div.bet-wrap", LoadBookieBank);
            setTitleIfBookieBank(document.location.href);
            checkBookieTimers();
        } else if (document.location.href.match(/\/bazaar.php*/)) {
            waitForKeyElements("div.bazaar-page-wrap", checkBookieTimers);
        } else if (document.location.href.match(/\/imarket.php*/)) {
            waitForKeyElements("div.main-market-page", checkBookieTimers);
        } else if (document.location.href.match(/\/events.php*/)) {
            waitForKeyElements("div.mailbox-container", checkBookieTimers);
        } else if (document.location.href.match(/\/preferences.php*/)) {
            createSettingsPage();
            checkBookieTimers();
        } else {
            checkBookieTimers();
        }
    } else if (document.location.href.match(/\/preferences.php*/)) {
        createSettingsPage();
    } else {}
})();
