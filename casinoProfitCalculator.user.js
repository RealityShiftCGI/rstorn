// ==UserScript==
// @name         Casino Stat Calculator
// @namespace    realityShiftCasinoCalculator
// @version      0.12
// @description  Calculates profit on casino pages
// @author       RealityShift [2001304]
// @match        *://*.torn.com/loader.php?sid=viewBlackjackStats
// @match        *://*.torn.com/loader.php?sid=rouletteStatistics
// @match        *://*.torn.com/loader.php?sid=viewHighLowStats
// @match        *://*.torn.com/loader.php?sid=viewSlotsStats
// @match        *://*.torn.com/loader.php?sid=viewKenoStats
// @match        *://*.torn.com/loader.php?sid=viewRussianRouletteStats
// @match        *://*.torn.com/loader.php?sid=viewRussianRouletteLastGames
// @match        *://*.torn.com/loader.php?sid=viewCrapsStats
// @updateURL	 http://realityshift-torn.s3-website.us-east-2.amazonaws.com/Public/casinoProfitCalculator.user.js
// @downloadURL	 http://realityshift-torn.s3-website.us-east-2.amazonaws.com/Public/casinoProfitCalculator.user.js
// @grant        none
// ==/UserScript==
// May 26, 2018: Initial implementation.
// May 26, 2018: Fixed calculations, rr pot division, and added updateURLs

(function() {
    'use strict';
    let index, color, profit, wrapName;

    /**
     * Calculates current profit for given casino game
     * @param {integer} index the starting index of the object gaining profit gain on the casino game.
     * @returns {integer} the total profit over past 20 games
     */
    function calculateCurrentProfit(index) {
        let moneyGain = $('#your-stats > ul > li:nth-child(' + index + ') > ul > li.stat-value').text().trim();
        moneyGain = moneyGain.replace("$","");
        moneyGain = moneyGain.replace(/,/g,"");

        let moneyLoss = $('#your-stats > ul > li:nth-child(' + (index + 1) + ') > ul > li.stat-value').text().trim();
        moneyLoss = moneyLoss.replace("$","");
        moneyLoss = moneyLoss.replace(/,/g,"");

        return (numberWithCommas(parseInt(moneyGain) - parseInt(moneyLoss)));
    }

    /**
     * Loops through all child objects of the most recent 20 games and returns profit.
     * @returns {integer} the total profit over past 20 games
     */
    function calculateLastRrGamesProfit() {
        const childItems = $('#mainContainer > div.content-wrapper.m-left20.left.spring > div.roulette-last-rolls-wrap > ul').children();
        let profit = 0;

        for (let count = 1; count <= childItems.length; count++) {
            var result = $('#mainContainer > div.content-wrapper.m-left20.left.spring > div.roulette-last-rolls-wrap > ul > li:nth-child(' + count + ') > ul > li.won-status')[0].innerText;
            var potWithSign = $('#mainContainer > div.content-wrapper.m-left20.left.spring > div.roulette-last-rolls-wrap > ul > li:nth-child(' + count + ') > div > ul > li:nth-child(1)')[0].innerText
            let potAsNumber = potWithSign.replace("$", "");
            potAsNumber = potAsNumber.replace(/,/g,"");

            if (result === "Won") {
                profit += parseInt(potAsNumber);
            } else if (result === "Loss" ) {
                profit -= parseInt(potAsNumber);
            }
        }

        profit = parseInt(profit) / 2; // RR shows pot, not profit.

        return (numberWithCommas(profit));
    }

    /**
     * Displays info notice on the page
     * @param {string} msg The message to be in the info panel.
     * @param {string} id ID of the info panel.
     * @param {string} color Color for the panel.
     * @param {string} idName ID for the div.
     */
    function createInfoNotice(msg, id, color, idName) {
        return ($('<div class="info-msg-cont border-round m-top10 ' + color + '" id="' + id + '"><div class="info-msg border-round">' +
            '<i class="info-icon"></i><div class="delimiter"><div id="' + idName + '" class="msg right-round">' + msg +
            '</div></div></div></div>'));
    }

    /**
     * Displays info notice on the page
     * @param {integer} x An integer to format.
     * @returns {string} The integer in $x,xxx,xxx format
     */
    function numberWithCommas(x) {
        let parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    if (document.location.href.match(/\/*viewBlackjackStats/)) {
        index = 5;
    } else if (document.location.href.match(/\/*viewRussianRouletteLastGames/)) {
        index = 0;
    } else if (document.location.href.match(/\/*viewRussianRouletteStats/)) {
        index = 8;
    } else {
        index = 8;
    }

    if (index !== 0) {
        profit = calculateCurrentProfit(index);
        wrapName = ".stats-main-wrap";
    } else {
        profit = calculateLastRrGamesProfit()
        wrapName = ".roulette-last-rolls-wrap";
    }

    if (parseInt(profit) > 0) {
        color = "green";
    } else {
        color = "red";
    }

    const msg = "Your current profit is: $" + profit;
    (createInfoNotice(msg, 'profitFrame', color, 'info')).insertBefore(wrapName);
})();