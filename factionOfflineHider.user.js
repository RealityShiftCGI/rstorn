// ==UserScript==
// @name         Faction Offline Filter
// @namespace    realityShiftFactionFilter
// @version      1.0
// @description  Hides all players that are offline on the faction page.
// @author       RealityShift [2001304]
// @require      https://gist.github.com/raw/2625891/waitForKeyElements.js
// @match        *://*.torn.com/factions.php*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function hideOwnFaction() {
        const factionMembers = $('#faction-info > div.faction-info-wrap.another-faction > div > ul.member-list.info-members.bottom-round.t-blue-cont.h').children();

        for (let ctr = 1; ctr <= factionMembers.length; ctr++) {
            const icon = $('#faction-info > div.faction-info-wrap.another-faction > div > ul.member-list.info-members.bottom-round.t-blue-cont.h > li:nth-child(' + ctr + ') > div.member.icons > #iconTray > #icon2')[0];

            if (icon && icon.title && icon.title.indexOf("Offline") > -1) {
                $('#faction-info > div.faction-info-wrap.another-faction > div > ul.member-list.info-members.bottom-round.t-blue-cont.h > li:nth-child(' + ctr + ')')[0].hidden = true;
            }
        }
    }

    function hideOtherFaction() {
        const factionMembers = $('#factions > div:nth-child(8) > div > ul.member-list.info-members.bottom-round.t-blue-cont.h').children();

        for (let ctr = 1; ctr <= factionMembers.length; ctr++) {
            const icon = $('#factions > div:nth-child(8) > div > ul.member-list.info-members.bottom-round.t-blue-cont.h > li:nth-child(' + ctr + ') > div.member.icons > #iconTray > #icon2')[0];

            if (icon && icon.title && icon.title.indexOf("Offline") > -1) {
                $('#factions > div:nth-child(8) > div > ul.member-list.info-members.bottom-round.t-blue-cont.h > li:nth-child(' + ctr + ')')[0].hidden = true;
            }
        }
    }

    if (document.location.href.match(/factions.php\?step=your#\/tab=info/)) {
        waitForKeyElements("div.faction-info-wrap", hideOwnFaction);
    } else if (document.location.href.match(/factions.php\?step=profile*/)) {
        waitForKeyElements("div.faction-info-wrap", hideOtherFaction);
    }
})();