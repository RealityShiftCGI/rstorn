// ==UserScript==
// @name         ActionTracker
// @namespace    realityActionTracker
// @version      1.03
// @description  Creates info panel for tracked users
// @author       RealityShift[2001304]
// @include      *.torn.com/*
// @require      https://code.jquery.com/jquery-2.2.0.min.js
// @require      https://gist.github.com/raw/2625891/waitForKeyElements.js
// @updateURL	 https://s3.us-east-2.amazonaws.com/realityshift-torn/Public/actionTracker.user.js
// @downloadURL	 https://s3.us-east-2.amazonaws.com/realityshift-torn/Public/actionTracker.user.js
// ==/UserScript==
// June 29 2018 - Added error message output to catch incorrect keys or other API errors.

(function() {
    "use strict";

    // Declare Variables
    const API_KEY = localStorage.rsApiKey != null ? localStorage.rsApiKey : "key"; // UPDATE KEY HERE IF YOU DON'T HAVE ONE STORED
    const MIN_TIME_TRACK = 0; // The shortest time in minutes of user activity to track. Values valid 0 through 59.
    const MAX_TIME_TRACK = 5; // The longest time in minutes of user activity to track. Values valid 0 through 59.

    // Targets to track. Must be in quotes and comma separated. NOTE: Each user is an API call, use at your own discretion.
    const targetUsers = [
        "2001304"
    ];
    let infoCreated = false;

    function createInfoNotice() {
        return ($('<div class="info-msg-cont border-round m-top10 green" id=notifier><div class="info-msg border-round">' +
            '<i class="info-icon"></i><div class="delimiter"><div id=divnotifier class="msg right-round"></div></div></div></div>'));
    }

    localStorage.rsApiKey = API_KEY;

    function mainScript() {
        if (API_KEY !== "" && targetUsers !== "") {

            let message = "";
            targetUsers.forEach(function(user) {
                const url = "https://api.torn.com/user/" + user + "?selections=&key=" + API_KEY;

                $.ajax({
                    url: url,
                    cache: false,
                    success: function(data) {
                        const lastAction = data.last_action;
                        if (data && data.error) {
                            createInfoNotice().insertAfter('.content-title');
                            $('#divnotifier')[0].innerHTML = "ERROR: " + data.error.code + " " + data.error.error;
                        } else if (lastAction.indexOf("minute") > -1) {
                            const last = parseInt(lastAction.match(/\d+/));
                            console.log("last:", last);
                            if (last >= MIN_TIME_TRACK && last < MAX_TIME_TRACK) {
                                if (!infoCreated) {
                                    createInfoNotice().insertAfter('.content-title');
                                    infoCreated = true;
                                }

                                message += "<a href=https://www.torn.com/profiles.php?XID=" + data.player_id + ">" + data.name + " [" + data.player_id + "] </a> was online " + data.last_action + ". Status: " + data.status[0] + "<br>";
                                $('#divnotifier')[0].innerHTML = message;
                            }
                        }
                    }
                });
            });
        } else if (API_KEY === "") {
            createInfoNotice().insertAfter('.content-title');
            $('#divnotifier')[0].innerHTML = "Add your API key in the NotifyUser script";
        } else if (targetUsers.length === 0) {
        } else {
            createInfoNotice().insertAfter('.content-title');
            $('#divnotifier')[0].innerHTML = "Something unintended went wrong" ;
        }
    }

    if (document.location.href.match(/\/bookie.php*/)) {
        waitForKeyElements("div.bet-wrap", mainScript);
    } else if (document.location.href.match(/\/bazaar.php*/)) {
        waitForKeyElements("div.bazaar-page-wrap", mainScript);
    } else if (document.location.href.match(/\/imarket.php*/)) {
        waitForKeyElements("div.main-market-page", mainScript);
    } else if (document.location.href.match(/\/events.php*/)) {
        waitForKeyElements("div.mailbox-container", mainScript);
    } else {
        mainScript()
    }
})();
